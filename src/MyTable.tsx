import React from "react";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import PropTypes from "prop-types";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

interface Props {
  rows: Array<{
    id: string;
    modelo: string;
    marca: string;
    año: number;
    dueño: string;
    precio: number;
  }>;
}

export function MyTable({ rows }: Props) {
  return (
    <Paper>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Modelo</TableCell>
            <TableCell>Marca</TableCell>
            <TableCell>año</TableCell>
            <TableCell>dueño</TableCell>
            <TableCell>precio</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => (
            <TableRow key={row.id}>
              <TableCell>{row.modelo}</TableCell>
              <TableCell>{row.marca}</TableCell>
              <TableCell>{row.año}</TableCell>
              <TableCell>{row.dueño}</TableCell>
              <TableCell>{row.precio}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
}
