import React, { useState } from "react";
import { generate } from "shortid";
import { MyForm } from "./MyForm";
import { MyTable } from "./MyTable";
import Grid from '@material-ui/core/Grid'

const App = () => {
  const [rows, setRows] = useState([
    {
      id: " ",
      modelo: " ",
      marca: " ",
      año: 0 ,
      dueño: " ",
      precio: 0
    }
  ]);

  return (
    <Grid container justify={"center"} direction="row"  alignItems={"center"} spacing={3}>
      <Grid item md={2}></Grid>
      <Grid item md={4}>
          <MyForm
            onSubmit={data => {
              setRows(currentRows => [
                {
                  id: generate(),
                  ...data
                },
                ...currentRows
              ]);
            }}
          />       
      </Grid>
      <Grid item md={6}>
        <MyTable rows={rows} />
      </Grid>
      
     
    </ Grid>
  );
};

export default App;
