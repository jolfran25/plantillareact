import { Button } from "@material-ui/core";
//import TextField from '@material-ui/core/TextField'
import { Form, Formik, Field} from "formik";
import * as React from "react";

import { MyField } from "./MyField";

interface Values {
  modelo: string;
  marca: string;
  año: number;
  dueño: string;
  precio: number;
}

interface Props {
  onSubmit: (values: Values) => void;
}

export const MyForm: React.FC<Props> = ({ onSubmit }) => {
  
  return (
    <Formik
      initialValues={{ modelo: "", marca: "", año: 0 , dueño: "", precio: 0 }}
      onSubmit={(values, { resetForm }) => {
        onSubmit(values);
        resetForm();
      }}
    >
      {({ values }) => (
        <Form>
          <div>
            <Field name="modelo" placeholder="Modelo" component={MyField} />
          </div>
          <div>
            <Field name="marca" placeholder="Marca" component={MyField} />
          </div>
          <div>
            <Field name="año" placeholder="año" component={MyField} />
          </div>
          <div>
            <Field name="dueño" placeholder="Dueño" component={MyField} />
          </div>
          <div>
            <Field name="precio" placeholder="Precio" component={MyField} />
          </div>
          <Button type="submit">submit</Button>
          
        </Form>
      )}
    </Formik>
  );
};
